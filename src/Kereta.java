public class Kereta {
    private Tiket tiketKomuter, tiketKAJJ;
    private String namaKereta;
    private int jumlahTiket;

    public Kereta() {
        tiketKomuter = new Tiket();
        this.namaKereta = "komuter";
    }

    public Kereta(String namaKereta, int jumlahTiket) {
        tiketKAJJ = new Tiket(namaKereta, jumlahTiket);
        this.namaKereta = namaKereta;
        this.jumlahTiket = jumlahTiket;
    }

    public void tambahTiket(String namaPenumpang) {
        //Memasukkan parameter namaPenumpang kedalam array namaPenumpang pada kelas Tiket
        String[] daftarPenumpang = tiketKomuter.getNamaPenumpang();
        String[] newDaftarPenumpang = new String[daftarPenumpang.length + 1];
        for (int i = 0; i < daftarPenumpang.length; i++) {
            newDaftarPenumpang[i] = daftarPenumpang[i];
        }
        newDaftarPenumpang[newDaftarPenumpang.length - 1] = namaPenumpang;
        tiketKomuter.setNamaPenumpang(newDaftarPenumpang);
    }

    public void tambahTiket(String namaPenumpang, String asal, String tujuan) {
        //Memasukkan parameter namaPenumpang kedalam String array namaPenumpang pada kelas Tiket
        String[] daftarPenumpang = tiketKAJJ.getNamaPenumpang();
        String[] newDaftarPenumpang = new String[daftarPenumpang.length + 1];
        for (int i = 0; i < daftarPenumpang.length; i++) {
            newDaftarPenumpang[i] = daftarPenumpang[i];
        }
        newDaftarPenumpang[newDaftarPenumpang.length - 1] = namaPenumpang;
        tiketKAJJ.setNamaPenumpang(newDaftarPenumpang);

        //Memasukkan parameter asal kedalam String array asal pada kelas Tiket
        String[] daftarAsal = tiketKAJJ.getAsal();
        String[] newDaftarAsal = new String[daftarAsal.length + 1];
        for (int i = 0; i < daftarAsal.length; i++) {
            newDaftarAsal[i] = daftarAsal[i];
        }
        newDaftarAsal[newDaftarAsal.length - 1] = asal;
        tiketKAJJ.setAsal(newDaftarAsal);

        //Memasukkan parameter tujuan kedalam String array tujuan pada kelas Tiket
        String[] daftarTujuan= tiketKAJJ.getTujuan();
        String[] newDaftarTujuan = new String[daftarTujuan.length + 1];
        for (int i = 0; i < daftarTujuan.length; i++) {
            newDaftarTujuan[i] = daftarTujuan[i];
        }
        newDaftarTujuan[newDaftarTujuan.length - 1] = tujuan;
        tiketKAJJ.setTujuan(newDaftarTujuan);
    }

    public void tampilkanTiket() {
        if (this.namaKereta.equals("komuter")) {
            String[] daftarPenumpang = tiketKomuter.getNamaPenumpang();
            if (daftarPenumpang.length > 1000) {
                System.out.println("Tiket kereta komuter sudah habis");
            } else {
                for (int i = 0; i < daftarPenumpang.length; i++) {
                    System.out.println("==================================================");
                    System.out.println("Tiket berhasil dipesan");
                }
                System.out.println("==================================================");
                System.out.println("Daftar penumpang kereta api komuter:");
                System.out.println("----------------------------");
                for (String nama : daftarPenumpang) {
                    System.out.println("Nama: " + nama);
                }
            }
        } else {
            String[] daftarPenumpang = tiketKAJJ.getNamaPenumpang();
            String[] daftarAsal = tiketKAJJ.getAsal();
            String[] daftarTujuan = tiketKAJJ.getTujuan();

            for (int i = 0; i < daftarPenumpang.length; i++){
                int sisa = this.jumlahTiket - i - 1;
                System.out.println("==================================================");
                System.out.println("Tiket berhasil dipesan Sisa tiket tersedia: " + sisa );
                if ( sisa < 1){
                    System.out.println("==================================================");
                    System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
                    break;
                }
            }
            System.out.println("==================================================");
            System.out.println("Daftar penumpang kereta api " + this.namaKereta);
            System.out.println("----------------------------");
            for (int i = 0; i < daftarPenumpang.length; i++) {
                int sisa = this.jumlahTiket - i - 1;
                System.out.println("Nama: " + daftarPenumpang[i]);
                System.out.println("Asal: " + daftarAsal[i]);
                System.out.println("Tujuan: " + daftarTujuan[i]);
                System.out.println("----------------------------");
                if (sisa < 1){
                    break;
                }
            }
        }
    }
}
